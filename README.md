# SSH private key

### Usage

- Generate a key: `ssh-keygen -t ed25519 -f key`
- Add the public key to authorized_keys file for each host you want to deploy to
- Create a SSH_PRIVATE_KEY variable in CI/CD settings with the generated private key
- Create a HOSTS variable with a list of hosts you want to deploy to

### Example deploy job:

``` yaml
include: 'https://gitlab.cern.ch/soc/ssh-private-key/raw/master/ssh.yml'

deploy:
 stage: deploy
 extends:
   - .deploy
 script:
   - >
     for HOST in $HOSTS; do
       rsync -a directory hostname@$HOST:/deploy_path
     done
```

See the detailed description [https://docs.gitlab.com/ee/ci/ssh_keys/](https://docs.gitlab.com/ee/ci/ssh_keys/)
